#!/usr/bin/env python3
import pika
import os
import cv2
import numpy as np

credentials = pika.PlainCredentials('rabbitmq', 'rabbitmq')
connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.0.143', 5672, '/', credentials))
channel = connection.channel()

channel.queue_declare(queue='list_of_images_to_process')

path = os.getcwd()
output = path + '/output'
dataset = path + '/dataset'


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    name = str(body)[2:-1]
    # Scale image x2
    img = cv2.imread(dataset + "/" + name)
    res = cv2.resize(img, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
    cv2.imwrite(output + "/" + name, res)
    # Rotate image
    img = cv2.imread(dataset + "/" + name, 0)
    rows, cols = img.shape
    M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 90, 1)
    dst = cv2.warpAffine(img, M, (cols, rows))
    cv2.imwrite(output + "/" + name, dst)


channel.basic_consume(
    queue='list_of_images_to_process', on_message_callback=callback, auto_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
