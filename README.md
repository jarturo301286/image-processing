The Project scales and rotates images form a dataset taken from
https://commons.wikimedia.org/wiki/Category:Curtis%27s_Botanical_Magazine,_Volume_26
The application has two executable files, setup.py and process.py
setup.py creates the input and output directories (./dataset, ./output), downloads the images from source to the /dataset directory and initializes a rabbitmq message queue for the processes (process.py) to read from.
process.py consumes the queue, scale, rotates the images corresponding to the message queue and save the processed images in the output
directory
One or more process.py executable file can be instantiated at once allowing  parallel image processing without corrupting the output or input data. As the massage queue is been consumed by the process.py’s the images are getting processed and outputted.
The docker_file_base_image_setup is used to package setup.py and setup all the python dependencies  of the application
The Dockerfile is used to package process.py and later run it within docker-compose env
This gives flexibility and allows easier deployments in multiple environments using only the images created and advantages of gitlab CI/CD

To start the application
From terminal:
open terminal cd to project folder
- ./setup.py
- ./process.py

 OR
- docker build -t setup:0.1  --file dockerfile_base_image_setup .
- docker build -t image_proces:0.5 .
- docker-compose up -d
