#!/usr/bin/env python3
import os
from bs4 import BeautifulSoup
import requests
import urllib.request
import pika

# rabbitmq connection setup
credentials = pika.PlainCredentials('rabbitmq', 'rabbitmq')
#192.168.0.143
connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.0.143', 5672, '/', credentials))
channel = connection.channel()
channel.queue_declare(queue='list_of_images_to_process')
# end rabbitmq connection send

# Get current dir
path = os.getcwd()
print("The current working directory is %s" % path)
# define the name of the directories to be created
dataset = path + '/dataset'
try:
    os.mkdir(dataset)
except OSError:
    print("Creation of the directory %s failed" % dataset)
else:
    print("Successfully created the directory %s " % dataset)

output = path + '/output'
try:
    os.mkdir(output)
except OSError:
    print("Creation of the directory %s failed" % output)
else:
    print("Successfully created the directory %s " % output)

# Get images from url
source_url = "https://commons.wikimedia.org/wiki/Category:Curtis%27s_Botanical_Magazine,_Volume_26"
response = requests.get(source_url)
# Make string object from response content
soup = BeautifulSoup(response.content, 'html.parser')


# Download image from url and name it from source
def download_image(url, name):
    urllib.request.urlretrieve(url, dataset + "/" + str(name))

# Finds the tags with the images sources, calls download method and writes to message queue in rabbit
for link in soup.find_all("a", class_="image"):
    img = link.find_all("img")
    src = img[0].get('src')
    print(src)
    urlDownload = "https://commons.wikimedia.org/"
    tempUrl = urlDownload + link.get('href')
    print(tempUrl)
    filename = tempUrl.split("/")[-1]
    print('FILE NAME ' + filename)
    download_image(src, filename)
    rmq = filename
    channel.basic_publish(exchange='',
                          routing_key='list_of_images_to_process',
                          body=str(rmq))
    print("Sent" + str(rmq))

# Close rabbitmq connection
connection.close()

